using System;
using System.Net;
using System.Text.Json;
using System.Threading.Tasks;
using Timesheet.NETCore.Exceptions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Timesheet.NETCore.Models;

namespace Timesheet.NETCore.Middlewares
{
    public class ErrorHandlingMiddleware
    {
        private readonly RequestDelegate _next;

        public ErrorHandlingMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context, IWebHostEnvironment env)
        {
            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(context, ex);
            }
        }

        private static Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            ErrorModel error = new ErrorModel();
            var exceptionType = exception.GetType();
            if (exceptionType == typeof(EntityNotFoundException))
            {
                error.message = "Not Found - The requested resource was not found on this server.";
                error.statusCode = HttpStatusCode.NotFound;
            }
            else if (exceptionType == typeof(BadRequestException))
            {
                error.message = "Bad Request - Unsuccessful attempt to send data.";
                error.statusCode = HttpStatusCode.BadRequest;
            }
            else
            {
                error.message = exception.Message;
                error.statusCode = HttpStatusCode.InternalServerError;
            }

            var result = JsonSerializer.Serialize(new { error });
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)error.statusCode;
            return context.Response.WriteAsync(result);
        }
    }
}