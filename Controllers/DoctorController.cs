using Microsoft.AspNetCore.Mvc;
using Timesheet.NETCore.Models;
using Timesheet.NETCore.Services;


namespace Timesheet.NETCore.Controllers
{
    [Route("/doctor")]
    [ApiController]
    public class DoctorController : ControllerBase
    {
        public readonly IDoctorService _doctorService;
        public DoctorController(IDoctorService doctorService)
        {
            _doctorService = doctorService;
        }

        [HttpPost("")]
        public ActionResult<Doctor> createDoctor(Doctor doctor)
        {
            return Ok(_doctorService.createDoctor(doctor));
        }

        [HttpGet("")]
        public ActionResult<Doctor> getAllDoctors()
        {
            return Ok(_doctorService.getAllDoctors());
        }

        [HttpGet("{id}")]
        public ActionResult<Doctor> getDoctorByID(int id)
        {
            return Ok(_doctorService.getDoctorByID(id));
        }

        [HttpDelete("{id}")]
        public ActionResult deleteDoctor(int id)
        {
            _doctorService.deleteDoctor(id);
            return Ok();
        }

        [HttpPut("")]
        public ActionResult updateDoctor(Doctor doctor)
        {
            _doctorService.updateDoctor(doctor);
            return Ok();
        }
    }
}
