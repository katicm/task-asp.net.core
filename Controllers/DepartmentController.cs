using Microsoft.AspNetCore.Mvc;
using Timesheet.NETCore.Models;
using Timesheet.NETCore.Services;


namespace Timesheet.NETCore.Controllers
{
    [Route("/department")]
    [ApiController]
    public class DepartmentController : ControllerBase
    {
        public readonly IDepartmentService _departmentService;
        public DepartmentController(IDepartmentService departmentService)
        {
            _departmentService = departmentService;
        }

        [HttpPost("")]
        public ActionResult<Department> createDepartment(Department department)
        {
            return Ok(_departmentService.createDepartment(department));
        }

        [HttpGet("")]
        public ActionResult<Department> getAllDepartments()
        {
            return Ok(_departmentService.getAllDepartments());
        }

        [HttpGet("{id}")]
        public ActionResult<Department> getDepartmentByID(int id)
        {
            return Ok(_departmentService.getDepartmentByID(id));
        }

        [HttpDelete("{id}")]
        public ActionResult deleteDepartment(int id)
        {
            _departmentService.deleteDepartment(id);
            return Ok();
        }

        [HttpPut("")]
        public ActionResult updateDepartment(Department department)
        {
            _departmentService.updateDepartment(department);
            return Ok();
        }
    }
}
