using Microsoft.AspNetCore.Mvc;
using Timesheet.NETCore.Models;
using Timesheet.NETCore.Services;


namespace Timesheet.NETCore.Controllers
{
    [Route("/treatment")]
    [ApiController]
    public class TreatmentController : ControllerBase
    {
        public readonly ITreatmentService _treatmentService;
        public TreatmentController(ITreatmentService treatmentService)
        {
            _treatmentService = treatmentService;
        }

        [HttpGet("patient={patientID}")]
        public ActionResult<TreatmentExtend> getTreatmentsExtendByPatient(int patientID)
        {
            return Ok(_treatmentService.getTreatmentsExtendByPatient(patientID));
        }
        [HttpGet("doctor{doctorID}")]
        public ActionResult<TreatmentExtend> getTreatmentsExtendByDoctor(int doctorID)
        {
            return Ok(_treatmentService.getTreatmentsExtendByDoctor(doctorID));
        }

        [HttpGet("extend")]
        public ActionResult<TreatmentExtend> getAllTreatmentsExtend()
        {
            return Ok(_treatmentService.getAllTreatmentsExtend());
        }

        [HttpPost("")]
        public ActionResult<Treatment> createTreatment(Treatment treatment)
        {
            return Ok(_treatmentService.createTreatment(treatment));
        }

        [HttpGet("")]
        public ActionResult<Treatment> getAllTreatments()
        {
            return Ok(_treatmentService.getAllTreatments());
        }

        [HttpGet("{id}")]
        public ActionResult<Treatment> getTreatmentByID(int id)
        {
            return Ok(_treatmentService.getTreatmentByID(id));
        }

        [HttpDelete("{id}")]
        public ActionResult deleteTreatment(int id)
        {
            _treatmentService.deleteTreatment(id);
            return Ok();
        }

        [HttpPut("")]
        public ActionResult updateTreatment(Treatment treatment)
        {
            _treatmentService.updateTreatment(treatment);
            return Ok();
        }
    }
}
