using Microsoft.AspNetCore.Mvc;
using Timesheet.NETCore.Models;
using Timesheet.NETCore.Services;


namespace Timesheet.NETCore.Controllers
{
    [Route("/appointment")]
    [ApiController]
    public class AppointmentController : ControllerBase
    {
        public readonly IAppointmentService _appointmentService;
        public AppointmentController(IAppointmentService appointmentService)
        {
            _appointmentService = appointmentService;
        }

        [HttpPost("")]
        public ActionResult<Appointment> createAppointment(Appointment appointment)
        {
            return Ok(_appointmentService.createAppointment(appointment));
        }

        [HttpGet("")]
        public ActionResult<Appointment> getAllAppointments()
        {
            return Ok(_appointmentService.getAllAppointments());
        }

        [HttpGet("{id}")]
        public ActionResult<Appointment> getAppointmentByID(int id)
        {
            return Ok(_appointmentService.getAppointmentByID(id));
        }

        [HttpDelete("{id}")]
        public ActionResult deleteAppointment(int id)
        {
            _appointmentService.deleteAppointment(id);
            return Ok();
        }

        [HttpPut("")]
        public ActionResult updateAppointment(Appointment appointment)
        {
            _appointmentService.updateAppointment(appointment);
            return Ok();
        }
    }
}
