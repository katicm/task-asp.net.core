using Microsoft.AspNetCore.Mvc;
using Timesheet.NETCore.Models;
using Timesheet.NETCore.Services;


namespace Timesheet.NETCore.Controllers
{
    [Route("/visit")]
    [ApiController]
    public class VisitController : ControllerBase
    {
        public readonly IVisitService _visitService;
        public VisitController(IVisitService visitService)
        {
            _visitService = visitService;
        }

        [HttpPost("")]
        public ActionResult<Visit> createVisit(Visit visit)
        {
            return Ok(_visitService.createVisit(visit));
        }

        [HttpGet("")]
        public ActionResult<Visit> getAllVisits()
        {
            return Ok(_visitService.getAllVisits());
        }

        [HttpGet("{id}")]
        public ActionResult<Visit> getVisitByID(int id)
        {
            return Ok(_visitService.getVisitByID(id));
        }

        [HttpDelete("{id}")]
        public ActionResult deleteVisit(int id)
        {
            _visitService.deleteVisit(id);
            return Ok();
        }

        [HttpPut("")]
        public ActionResult updateVisit(Visit visit)
        {
            _visitService.updateVisit(visit);
            return Ok();
        }
    }
}
