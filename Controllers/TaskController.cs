using System;
using Microsoft.AspNetCore.Mvc;
using Timesheet.NETCore.Models;
using Timesheet.NETCore.Services;


namespace Timesheet.NETCore.Controllers
{
    [Route("/task")]
    [ApiController]
    public class TaskController : ControllerBase
    {
        public readonly ITaskService _taskService;
        public TaskController(ITaskService taskService)
        {
            _taskService = taskService;
        }

        [HttpPost("")]
        public ActionResult<TaskModel> createTask(TaskModel task)
        {
            return new OkObjectResult(_taskService.createTask(task));
            //return Ok(_taskService.createTask(task));
        }

        [HttpGet("date={date}")]
        public ActionResult<TaskModel> getTasksByDate(string date)
        {
            Console.WriteLine(date);
            return Ok(_taskService.GetTasksByDate(date));
        }

        [HttpGet("")]
        public ActionResult<TaskModel> getAllTasks()
        {
            return Ok(_taskService.GetAllTasks());
        }

        [HttpGet("id/{id:int}")]
        public ActionResult<TaskModel> getTaskByID(int id)
        {
            return Ok(_taskService.GetTaskByID(id));
        }
    }
}
