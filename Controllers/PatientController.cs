using Microsoft.AspNetCore.Mvc;
using Timesheet.NETCore.Models;
using Timesheet.NETCore.Services;


namespace Timesheet.NETCore.Controllers
{
    [Route("/patient")]
    [ApiController]
    public class PatientController : ControllerBase
    {
        public readonly IPatientService _patientService;
        public PatientController(IPatientService patientService)
        {
            _patientService = patientService;
        }

        [HttpPost("")]
        public ActionResult<Patient> createPatient(Patient patient)
        {
            return Ok(_patientService.createPatient(patient));
        }

        [HttpGet("")]
        public ActionResult<Patient> getAllPatients()
        {
            return Ok(_patientService.getAllPatients());
        }

        [HttpGet("{id}")]
        public ActionResult<Patient> getPatientByID(int id)
        {
            return Ok(_patientService.getPatientByID(id));
        }

        [HttpDelete("{id}")]
        public ActionResult deletePatient(int id)
        {
            _patientService.deletePatient(id);
            return Ok();
        }

        [HttpPut("")]
        public ActionResult updatePatient(Patient patient)
        {
            _patientService.updatePatient(patient);
            return Ok();
        }

        [HttpGet("extend")]
        public ActionResult<PatientExtend> getPatientsExtend()
        {
            return Ok(_patientService.getAllPatientsExtend());
        }
    }
}
