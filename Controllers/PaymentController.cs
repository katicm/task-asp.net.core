using Microsoft.AspNetCore.Mvc;
using Timesheet.NETCore.Models;
using Timesheet.NETCore.Services;


namespace Timesheet.NETCore.Controllers
{
    [Route("/payment")]
    [ApiController]
    public class PaymentController : ControllerBase
    {
        public readonly IPaymentService _paymentService;
        public PaymentController(IPaymentService paymentService)
        {
            _paymentService = paymentService;
        }

        [HttpPost("")]
        public ActionResult<Payment> createPayment(Payment payment)
        {
            return Ok(_paymentService.createPayment(payment));
        }

        [HttpGet("")]
        public ActionResult<Payment> getAllPayments()
        {
            return Ok(_paymentService.getAllPayments());
        }

        [HttpGet("{id}")]
        public ActionResult<Payment> getPaymentByID(int id)
        {
            return Ok(_paymentService.getPaymentByID(id));
        }

        [HttpDelete("{id}")]
        public ActionResult deletePayment(int id)
        {
            _paymentService.deletePayment(id);
            return Ok();
        }

        [HttpPut("")]
        public ActionResult updatePayment(Payment payment)
        {
            _paymentService.updatePayment(payment);
            return Ok();
        }
    }
}
