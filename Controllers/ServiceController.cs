using Microsoft.AspNetCore.Mvc;
using Timesheet.NETCore.Models;
using Timesheet.NETCore.Services;


namespace Timesheet.NETCore.Controllers
{
    [Route("/service")]
    [ApiController]
    public class ServiceController : ControllerBase
    {
        public readonly IServiceService _serviceService;
        public ServiceController(IServiceService serviceService)
        {
            _serviceService = serviceService;
        }

        [HttpPost("")]
        public ActionResult<Service> createService(Service service)
        {
            return Ok(_serviceService.createService(service));
        }

        [HttpGet("")]
        public ActionResult<Service> getAllServices()
        {
            return Ok(_serviceService.getAllServices());
        }

        [HttpGet("{id}")]
        public ActionResult<Service> getServiceByID(int id)
        {
            return Ok(_serviceService.getServiceByID(id));
        }

        [HttpDelete("{id}")]
        public ActionResult deleteService(int id)
        {
            _serviceService.deleteService(id);
            return Ok();
        }

        [HttpPut("")]
        public ActionResult updateService(Service service)
        {
            _serviceService.updateService(service);
            return Ok();
        }
    }
}
