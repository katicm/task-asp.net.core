using Microsoft.AspNetCore.Mvc;
using Timesheet.NETCore.Models;
using Timesheet.NETCore.Services;


namespace Timesheet.NETCore.Controllers
{
    [Route("/user")]
    [ApiController]
    public class UserController : ControllerBase
    {
        public readonly IUserService _userService;
        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpPost("")]
        public ActionResult<User> createUser(User user)
        {
            return Ok(_userService.createUser(user));
        }

        [HttpGet("")]
        public ActionResult<User> getAllUsers()
        {
            return Ok(_userService.getAllUsers());
        }

        [HttpGet("{id}")]
        public ActionResult<User> getUserByID(int id)
        {
            return Ok(_userService.getUserByID(id));
        }

        [HttpDelete("{id}")]
        public ActionResult deleteUser(int id)
        {
            _userService.deleteUser(id);
            return Ok();
        }

        [HttpPut("")]
        public ActionResult updateUser(User user)
        {
            _userService.updateUser(user);
            return Ok();
        }
    }
}
