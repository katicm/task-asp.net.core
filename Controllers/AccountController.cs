using System;
using Microsoft.AspNetCore.Mvc;
using Timesheet.NETCore.Models;
using Timesheet.NETCore.Services;


namespace Timesheet.NETCore.Controllers
{
    [Route("/account")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        public readonly IAccountService _accountService;
        public AccountController(IAccountService accountService)
        {
            _accountService = accountService;
        }

        [HttpPost("")]
        public ActionResult<Account> createAccount(Account account)
        {
            //return new OkObjectResult(_accountService.createAccount(account));
            return Ok(_accountService.createAccount(account));
        }

        [HttpGet("")]
        public ActionResult<Account> getAllAccounts()
        {
            return Ok(_accountService.getAllAccounts());
        }

        [HttpGet("{id}")]
        public ActionResult<Account> getAccountByID(int id)
        {
            return Ok(_accountService.getAccountByID(id));
        }

        [HttpDelete("{id}")]
        public ActionResult deleteAccount(int id)
        {
            _accountService.deleteAccount(id);
            return Ok();
        }

        [HttpPut("")]
        public ActionResult updateAccount(Account account)
        {
            _accountService.updateAccount(account);
            return Ok();
        }

        [HttpGet("login/email={email}&password={password}")]
        public ActionResult<Account> login(string email, string password)
        {
            return Ok(_accountService.login(email, password));
        }
    }
}
