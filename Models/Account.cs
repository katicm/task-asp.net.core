using System.ComponentModel.DataAnnotations;


namespace Timesheet.NETCore.Models
{
    public class Account
    {
        public int accountID { get; set; }
        [Required]
        public string email { get; set; }
        [Required]
        public string username { get; set; }
        [Required]
        public string role { get; set; }
        [Required]
        public string password { get; set; }
        [Required]
        public string created { get; set; }
        [Required]
        public string phone { get; set; }
    }
}