using System.ComponentModel.DataAnnotations;


namespace Timesheet.NETCore.Models
{
    public class Payment
    {
        public int paymentID { get; set; }

        [Required]
        public decimal cost { get; set; }

        [Required]
        public bool paid { get; set; }

        [Required]
        public string paidTime { get; set; }
    }
}