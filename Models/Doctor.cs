using System.ComponentModel.DataAnnotations;


namespace Timesheet.NETCore.Models
{
    public class Doctor
    {
        public int doctorID { get; set; }

        [Required]
        public int userID { get; set; }

        [Required]
        public int departmentID { get; set; }

        [Required]
        public string title { get; set; }
    }
}