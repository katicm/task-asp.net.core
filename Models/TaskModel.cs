using System.ComponentModel.DataAnnotations;


namespace Timesheet.NETCore.Models
{
    public class TaskModel
    {
        public int taskID { get; set; }

        [Required]
        public string title { get; set; }

        [Required]
        public int hours { get; set; }

        [Required]
        public string date { get; set; }
    }
}