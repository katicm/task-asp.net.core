using System.ComponentModel.DataAnnotations;


namespace Timesheet.NETCore.Models
{
    public class Service
    {
        public int serviceID { get; set; }

        [Required]
        public string name { get; set; }

        [Required]
        public int cost { get; set; }
        public int duration { get; set; }

    }
}