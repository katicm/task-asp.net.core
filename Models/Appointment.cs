using System.ComponentModel.DataAnnotations;


namespace Timesheet.NETCore.Models
{
    public class Appointment
    {
        public int appointmentID { get; set; }

        [Required]
        public int patientID { get; set; }

        [Required]
        public int doctorID { get; set; }

        [Required]
        public string scheduledDate { get; set; }

        public string notes { get; set; }
    }
}