using System.ComponentModel.DataAnnotations;


namespace Timesheet.NETCore.Models
{
    public class Department
    {
        public int departmentID { get; set; }
        [Required]
        public string name { get; set; }

    }
}