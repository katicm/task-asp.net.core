using System.ComponentModel.DataAnnotations;


namespace Timesheet.NETCore.Models
{
    public class Patient
    {
        public int patientID { get; set; }
        [Required]
        public int userID { get; set; }
        [Required]
        public string bloodGroup { get; set; }
        public string notes { get; set; }
        [Required]
        public string firstVisit { get; set; }
        [Required]
        public string lastVisit { get; set; }
        [Required]
        public string allergy { get; set; }
        public int visitCount { get; set; }
    }
    public class PatientExtend : Patient
    {
        public string fullPatientName { get; set; }
        public string middleName { get; set; }
        public string gender { get; set; }
        public string birthDate { get; set; }
        public string address { get; set; }
        public string email { get; set; }
        public string created { get; set; }
        public string phone { get; set; }
    }
}