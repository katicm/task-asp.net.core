using System.ComponentModel.DataAnnotations;


namespace Timesheet.NETCore.Models
{
    public class Treatment
    {
        public int treatmentID { get; set; }

        [Required]
        public int doctorID { get; set; }
        [Required]
        public int patientID { get; set; }
        [Required]
        public int serviceID { get; set; }
        [Required]
        public bool completed { get; set; }
        public string scheduledTime { get; set; }
        public string toothNo { get; set; }
        public string type { get; set; }
        public string zone { get; set; }

    }
    public class TreatmentExtend : Treatment
    {
        public string title { get; set; }
        public string doctorFullName { get; set; }
        public string procedure { get; set; }
        public int cost { get; set; }
        public int duration { get; set; }
    }
}