using System.ComponentModel.DataAnnotations;


namespace Timesheet.NETCore.Models
{
    public class User
    {
        public int userID { get; set; }

        [Required]
        public int accountID { get; set; }

        [Required]
        public string firstName { get; set; }

        public string middleName { get; set; }

        [Required]
        public string lastName { get; set; }

        [Required]
        public string gender { get; set; }

        [Required]
        public string birthDate { get; set; }

        public string address { get; set; }
    }
}