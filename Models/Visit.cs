using System.ComponentModel.DataAnnotations;


namespace Timesheet.NETCore.Models
{
    public class Visit
    {
        public int visitID { get; set; }

        [Required]
        public int appointmentID { get; set; }

        [Required]
        public int patientID { get; set; }

        [Required]
        public int doctorID { get; set; }

        public int paymentID { get; set; }

        public string datetime { get; set; }

        public string remark { get; set; }
    }
}