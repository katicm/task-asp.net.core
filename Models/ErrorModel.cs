using System.Net;


namespace Timesheet.NETCore.Models
{
    public class ErrorModel
    {
        public HttpStatusCode statusCode { get; set; }
        public string message { get; set; }
    }
}