
namespace Timesheet.NETCore.Consts
{
    public class SqlQuery
    {
        public string SELECT_ALL_TREATMENT_EXTEND_BY_DOCTORID = $"SELECT treatmentID, T.doctorID, D.title, U.firstName+' '+U.lastName as doctorFullName, patientID, T.serviceID, S.name as [procedure], completed, CONVERT(VARCHAR, scheduledTime, 126) as scheduledTime, toothNo, type, zone, cost, duration FROM POLY.[TREATMENT] as T " +
"LEFT JOIN POLY.DOCTOR as D on D.doctorID=T.doctorID LEFT JOIN POLY.[USER] as U on U.userID=D.userID LEFT JOIN POLY.SERVICE as S on S.serviceID=T.serviceID WHERE T.doctorID=@doctorID AND T.completed=0";
        public string SELECT_ALL_PATIENT_EXTEND = $"SELECT patientID, P.userID, bloodGroup, notes, CONVERT(NVARCHAR, firstVisit, 105) as firstVisit, CONVERT(NVARCHAR, lastVisit, 105) as lastVisit, allergy, visitCount, firstName+' '+lastName as fullPatientName, middleName, gender, CONVERT(VARCHAR, birthDate, 103) as birthDate, address, email, CONVERT(NVARCHAR, created, 105) as created, phone FROM POLY.[PATIENT] as P " +
        "LEFT JOIN POLY.[USER] as U on U.userID=P.userID LEFT JOIN POLY.ACCOUNT as A on A.accountID=U.accountID";
        public string SELECT_ALL_TREATMENT_EXTEND_BY_PATIENTID = $"SELECT treatmentID, T.doctorID, D.title, U.firstName+' '+U.lastName as doctorFullName, patientID, T.serviceID, S.name as [procedure], completed, CONVERT(VARCHAR, scheduledTime, 126) as scheduledTime, toothNo, type, zone, cost, duration FROM POLY.[TREATMENT] as T " +
        "LEFT JOIN POLY.DOCTOR as D on D.doctorID=T.doctorID LEFT JOIN POLY.[USER] as U on U.userID=D.userID LEFT JOIN POLY.SERVICE as S on S.serviceID=T.serviceID WHERE T.patientID=@patientID AND T.completed=0";
        public string SELECT_ALL_TREATMENT_EXTEND = $"SELECT treatmentID, T.doctorID, D.title, U.firstName+' '+U.lastName as doctorFullName, patientID, T.serviceID, S.name as [procedure], completed, CONVERT(VARCHAR, scheduledTime, 126) as scheduledTime, toothNo, type, zone, cost, duration FROM POLY.[TREATMENT] as T " +
        "LEFT JOIN POLY.DOCTOR as D on D.doctorID=T.doctorID LEFT JOIN POLY.[USER] as U on U.userID=D.userID LEFT JOIN POLY.SERVICE as S on S.serviceID=T.serviceID";
        public string CREATE_TREATMENT = $"INSERT INTO POLY.[TREATMENT] VALUES (@doctorID, @patientID, @serviceID, @completed, @scheduledTime, @toothNo, @type, @zone)";
        public string SELECT_LAST_TREATMENT = "SELECT TOP 1 treatmentID, doctorID, patientID, serviceID, completed, scheduledTime, toothNo, type, zone FROM POLY.[TREATMENT] ORDER BY treatmentID DESC";
        public string SELECT_ALL_TREATMENT = "SELECT treatmentID, doctorID, patientID, serviceID, completed, CONVERT(NVARCHAR, scheduledTime, 100) as scheduledTime, toothNo, type, zone FROM POLY.[TREATMENT]";
        public string SELECT_TREATMENT_BY_ID = "SELECT treatmentID, doctorID, patientID, serviceID, completed, scheduledTime, toothNo, type, zone FROM POLY.[TREATMENT] WHERE treatmentID=@id";
        public string UPDATE_TREATMENT = "UPDATE POLY.[TREATMENT] SET doctorId=@doctorID, patientID=@patientID, serviceID=@serviceID, completed=@completed, scheduledTime=@scheduledTime, toothNo=@toothNo, type=@type, zone=@zone WHERE treatmentID=@id";
        public string DELETE_TREATMENT = "DELETE FROM POLY.[TREATMENT] WHERE treatmentID=@id";
        public string CREATE_PAYMENT = $"INSERT INTO POLY.[PAYMENT] VALUES (@cost, @paid, @[paidTime])";
        public string SELECT_LAST_PAYMENT = "SELECT TOP 1 paymentID, cost, paid, CONVERT(NVARCHAR, [paidTime], 105) as [paidTime] FROM POLY.[PAYMENT] ORDER BY paymentID DESC";
        public string SELECT_ALL_PAYMENT = "SELECT paymentID, cost, paid, CONVERT(NVARCHAR, [paidTime], 105) as [paidTime] FROM POLY.[PAYMENT]";
        public string SELECT_PAYMENT_BY_ID = "SELECT paymentID, cost, paid, CONVERT(NVARCHAR, [paidTime], 105) as [paidTime] FROM POLY.[PAYMENT] WHERE paymentID=@id";
        public string UPDATE_PAYMENT = "UPDATE POLY.[PAYMENT] SET cost=@cost, paid=@paid, [paidTime]=@[paidTime] WHERE paymentID=@id";
        public string DELETE_PAYMENT = "DELETE FROM POLY.[PAYMENT] WHERE paymentID=@id";
        public string CREATE_APPOINTMENT = $"INSERT INTO POLY.[APPOINTMENT] VALUES (@patientID, @doctorID, @scheduledDate, @notes)";
        public string SELECT_LAST_APPOINTMENT = "SELECT TOP 1 appointmentID, patientID, doctorID, CONVERT(NVARCHAR, scheduledDate, 105) as scheduledDate, notes FROM POLY.[APPOINTMENT] ORDER BY appointmentID DESC";
        public string SELECT_ALL_APPOINTMENT = "SELECT appointmentID, patientID, doctorID, CONVERT(NVARCHAR, scheduledDate, 105) as scheduledDate, notes FROM POLY.[APPOINTMENT]";
        public string SELECT_APPOINTMENT_BY_ID = "SELECT appointmentID, patientID, doctorID, CONVERT(NVARCHAR, scheduledDate, 105) as scheduledDate, notes FROM POLY.[APPOINTMENT] WHERE appointmentID=@id";
        public string UPDATE_APPOINTMENT = "UPDATE POLY.[APPOINTMENT] SET patientID=@patientID, doctorID=@doctorID, scheduledDate=@scheduledDate, notes=@notes WHERE appointmentID=@id";
        public string DELETE_APPOINTMENT = "DELETE FROM POLY.[APPOINTMENT] WHERE appointmentID=@id";
        public string CREATE_VISIT = $"INSERT INTO POLY.[VISIT] VALUES (@appointmentID, @patientID, @doctorID, @paymentID, @datetime, @lastVisit, @remark)";
        public string SELECT_LAST_VISIT = "SELECT TOP 1 visitID, appointmentID, patientID, doctorID, paymentID, CONVERT(NVARCHAR, [datetime], 105) as [datetime], remark FROM POLY.[VISIT] ORDER BY visitID DESC";
        public string SELECT_ALL_VISIT = "SELECT visitID, appointmentID, patientID, doctorID, paymentID, CONVERT(NVARCHAR, [datetime], 105) as [datetime], remark FROM POLY.[VISIT]";
        public string SELECT_VISIT_BY_ID = "SELECT visitID, appointmentID, patientID, doctorID, paymentID, CONVERT(NVARCHAR, [datetime], 105) as [datetime], remark FROM POLY.[VISIT] WHERE visitID=@id";
        public string UPDATE_VISIT = "UPDATE POLY.[VISIT] SET appointmentID=@appointmentID, patientID=@patientID, doctorID=@doctorID, paymentID=@paymentID, [datetime]=@[datetime], remark=@remark WHERE visitID=@id";
        public string DELETE_VISIT = "DELETE FROM POLY.[VISIT] WHERE visitID=@id";
        public string CREATE_SERVICE = $"INSERT INTO POLY.[SERVICE] VALUES (@name, @cost, @duration)";
        public string SELECT_LAST_SERVICE = "SELECT TOP 1 * FROM POLY.[SERVICE] ORDER BY serviceID DESC";
        public string SELECT_ALL_SERVICE = "SELECT * FROM POLY.[SERVICE]";
        public string SELECT_SERVICE_BY_ID = "SELECT * FROM POLY.[SERVICE] WHERE serviceID=@id";
        public string UPDATE_SERVICE = "UPDATE POLY.[SERVICE] SET name=@name, cost=@cost, duration=@duration WHERE serviceID=@id";
        public string DELETE_SERVICE = "DELETE FROM POLY.[SERVICE] WHERE serviceID=@id";
        public string CREATE_DEPARTMENT = $"INSERT INTO POLY.[DEPARTMENT] VALUES (@name)";
        public string SELECT_LAST_DEPARTMENT = "SELECT TOP 1 * FROM POLY.[DEPARTMENT] ORDER BY departmentID DESC";
        public string SELECT_ALL_DEPARTMENT = "SELECT departmentID, name FROM POLY.[DEPARTMENT]";
        public string SELECT_DEPARTMENT_BY_ID = "SELECT * FROM POLY.[DEPARTMENT] WHERE departmentID=@id";
        public string UPDATE_DEPARTMENT = "UPDATE POLY.[DEPARTMENT] SET name=@name WHERE departmentID=@id";
        public string DELETE_DEPARTMENT = "DELETE FROM POLY.[DEPARTMENT] WHERE departmentID=@id";
        public string CREATE_DOCTOR = $"INSERT INTO POLY.[DOCTOR] VALUES (@userID, @departmentID, @title)";
        public string SELECT_LAST_DOCTOR = "SELECT TOP 1 doctorID, userID, departmentID, title FROM POLY.[DOCTOR] ORDER BY doctorID DESC";
        public string SELECT_ALL_DOCTOR = "SELECT doctorID, userID, departmentID, title FROM POLY.[DOCTOR]";
        public string SELECT_DOCTOR_BY_ID = "SELECT doctorID, userID, departmentID, title FROM POLY.[DOCTOR] WHERE doctorID=@id";
        public string UPDATE_DOCTOR = "UPDATE POLY.[DOCTOR] SET userID=@userID, departmentID=@departmentID, title=@title WHERE doctorID=@id";
        public string DELETE_DOCTOR = "DELETE FROM POLY.[DOCTOR] WHERE doctorID=@id";
        public string CREATE_PATIENT = $"INSERT INTO POLY.[PATIENT] VALUES (@userID, @bloodGroup, @notes, @firstVisit, @lastVisit, @allergy, @visitCount)";
        public string SELECT_LAST_PATIENT = "SELECT TOP 1 patientID, userID, bloodGroup, notes, CONVERT(NVARCHAR, firstVisit, 105) as firstVisit, CONVERT(NVARCHAR, lastVisit, 105) as lastVisit, allergy, visitCount FROM POLY.[PATIENT] ORDER BY patientID DESC";
        public string SELECT_ALL_PATIENT = "SELECT patientID, userID, bloodGroup, notes, CONVERT(NVARCHAR, firstVisit, 105) as firstVisit, CONVERT(NVARCHAR, lastVisit, 105) as lastVisit, allergy, visitCount FROM POLY.[PATIENT]";
        public string SELECT_PATIENT_BY_ID = "SELECT patientID, userID, bloodGroup, notes, CONVERT(NVARCHAR, firstVisit, 105) as firstVisit, CONVERT(NVARCHAR, lastVisit, 105) as lastVisit, allergy, visitCount FROM POLY.[PATIENT] WHERE patientID=@id";
        public string UPDATE_PATIENT = "UPDATE POLY.[PATIENT] SET userID=@userID, bloodGroup=@bloodGroup, notes=@notes, firstVisit=@firstVisit, lastVisit=@lastVisit, allergy=@allergy, visitCount=@visitCount WHERE patientID=@id";
        public string DELETE_PATIENT = "DELETE FROM POLY.[PATIENT] WHERE patientID=@id";
        public string CREATE_USER = $"INSERT INTO POLY.[USER] VALUES(@accountID, @firstName, @middleName, @lastName, @gender, @birthDate, @address)";
        public string SELECT_LAST_USER = "SELECT TOP 1 userID, accountID, firstName, middleName, lastName, gender, CONVERT(NVARCHAR, birthDate, 105) as birthDate, address FROM POLY.[USER] ORDER BY userID DESC";
        public string SELECT_ALL_USER = "SELECT userID, accountID, firstName, middleName, lastName, gender, CONVERT(NVARCHAR, birthDate, 105) as birthDate, address FROM POLY.[USER]";
        public string SELECT_USER_BY_ID = "SELECT userID, accountID, firstName, middleName, lastName, gender, CONVERT(NVARCHAR, birthDate, 105) as birthDate, address FROM POLY.[USER] WHERE userID=@id";
        public string UPDATE_USER = "UPDATE POLY.[USER] SET accountID=@accountID, firstName=@firstName, middleName=@middleName, lastName=@lastName, gender=@gender, birthDate=@birthDate, address=@address WHERE userID=@id";
        public string DELETE_USER = "DELETE FROM POLY.[USER] WHERE userID=@id";
        public string CREATE_ACCOUNT()
        {
            return $"INSERT INTO POLY.ACCOUNT VALUES(@email, @username, @role, @password, CURRENT_TIMESTAMP, @phone)";
        }
        public string SELECT_LAST_ACCOUNT()
        {
            return $"SELECT TOP 1 accountID,email,username,[role],password,convert(NVARCHAR, [created], 105) AS [created],phone FROM POLY.ACCOUNT ORDER BY accountID DESC";
        }
        public string SELECT_ALL_ACCOUNTS()
        {
            return $"SELECT accountID,email,username,[role],password,convert(NVARCHAR, created, 105) AS [created],phone FROM POLY.ACCOUNT";
        }
        public string SELECT_ACCOUNT_BY_ID()
        {
            return $"SELECT * FROM POLY.ACCOUNT WHERE POLY.ACCOUNT.accountID=";
        }
        public string UPDATE_ACCOUNT()
        {
            return $"UPDATE POLY.ACCOUNT SET email=@email, username=@username, role=@role, password=@password, phone=@phone WHERE accountId=@id";
        }
        public string DELETE_ACCOUNT()
        {
            return $"DELETE FROM POLY.ACCOUNT where ACCOUNT.accountID=@id";
        }
        public string LOGIN()
        {
            return $"SELECT * FROM POLY.ACCOUNT WHERE ACCOUNT.email=@email AND ACCOUNT.password=@password";
        }
        public string CREATE_TASK()
        {
            return $"INSERT INTO TIMESHEET.Task VALUES(@title, @hours, CONVERT(DATE, @date, 105))";
        }
        public string SELECT_LAST_TASK()
        {
            return $"SELECT TOP 1 taskID,title,hours,convert(NVARCHAR, [date], 105) AS [date] FROM TIMESHEET.Task ORDER BY taskID DESC";
        }
        public string SELECT_TASK_BY_DATE(string date)
        {
            return $"SELECT taskID,title,hours,convert(NVARCHAR, [date], 105) AS [date] FROM TIMESHEET.Task WHERE [date]=CONVERT(DATE,'{date}',105)";
        }
        public string SELECT_ALL_TASKS()
        {
            return $"SELECT taskID,title,hours,convert(NVARCHAR, [date], 105) AS [date] FROM TIMESHEET.Task";
        }
        public string SELECT_TASK_BY_ID(int taskID)
        {
            return $"SELECT taskID,title,hours,convert(NVARCHAR, [date], 105) AS [date] FROM TIMESHEET.Task WHERE taskID={taskID}";
        }
    }
}