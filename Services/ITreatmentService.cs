using Timesheet.NETCore.Models;
using System.Collections.Generic;

namespace Timesheet.NETCore.Services
{
    public interface ITreatmentService
    {
        Treatment createTreatment(Treatment treatment);
        void updateTreatment(Treatment treatment);
        void deleteTreatment(int treatmentID);
        List<Treatment> getAllTreatments();
        Treatment getTreatmentByID(int id);
        List<TreatmentExtend> getTreatmentsExtendByPatient(int patientID);
        List<TreatmentExtend> getTreatmentsExtendByDoctor(int doctorID);
        List<TreatmentExtend> getAllTreatmentsExtend();
    }
}