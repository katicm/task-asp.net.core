using Timesheet.NETCore.Models;
using System.Collections.Generic;

namespace Timesheet.NETCore.Services
{
    public interface IDepartmentService
    {
        Department createDepartment(Department department);
        void updateDepartment(Department department);
        void deleteDepartment(int departmentID);
        List<Department> getAllDepartments();
        Department getDepartmentByID(int id);
    }
}