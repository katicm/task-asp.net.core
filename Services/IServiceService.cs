using Timesheet.NETCore.Models;
using System.Collections.Generic;

namespace Timesheet.NETCore.Services
{
    public interface IServiceService
    {
        Service createService(Service service);
        void updateService(Service service);
        void deleteService(int serviceID);
        List<Service> getAllServices();
        Service getServiceByID(int id);
    }
}