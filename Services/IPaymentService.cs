using Timesheet.NETCore.Models;
using System.Collections.Generic;

namespace Timesheet.NETCore.Services
{
    public interface IPaymentService
    {
        Payment createPayment(Payment payment);
        void updatePayment(Payment payment);
        void deletePayment(int paymentID);
        List<Payment> getAllPayments();
        Payment getPaymentByID(int id);
    }
}