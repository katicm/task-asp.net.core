using Timesheet.NETCore.Models;
using System.Collections.Generic;

namespace Timesheet.NETCore.Services
{
    public interface IPatientService
    {
        Patient createPatient(Patient patient);
        void updatePatient(Patient patient);
        void deletePatient(int patientID);
        List<Patient> getAllPatients();
        Patient getPatientByID(int id);
        List<PatientExtend> getAllPatientsExtend();
    }
}