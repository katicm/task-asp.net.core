using Timesheet.NETCore.Models;
using System.Collections.Generic;

namespace Timesheet.NETCore.Services
{
    public interface IVisitService
    {
        Visit createVisit(Visit visit);
        void updateVisit(Visit visit);
        void deleteVisit(int visitID);
        List<Visit> getAllVisits();
        Visit getVisitByID(int id);
    }
}