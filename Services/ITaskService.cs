using Timesheet.NETCore.Models;
using System.Collections.Generic;

namespace Timesheet.NETCore.Services
{
    public interface ITaskService
    {
        TaskModel createTask(TaskModel task);
        List<TaskModel> GetTasksByDate(string date);
        List<TaskModel> GetAllTasks();
        TaskModel GetTaskByID(int id);
    }
}

