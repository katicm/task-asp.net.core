using Timesheet.NETCore.Exceptions;
using Timesheet.NETCore.Models;
using System;
using System.Collections.Generic;
using System.Data;
using Timesheet.NETCore.Consts;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;

namespace Timesheet.NETCore.Services.Implementation
{
    public class PaymentService : IPaymentService
    {
        private readonly string _connectionString;
        private SqlQuery _sqlQuery;
        public PaymentService(IConfiguration Configuration)
        {
            _connectionString = Configuration["ConnectionString:DBCS"];
            _sqlQuery = new SqlQuery();
        }
        public Payment createPayment(Payment payment)
        {
            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    using (var sqlCmd = new SqlCommand(_sqlQuery.CREATE_PAYMENT, connection))
                    {
                        sqlCmd.Parameters.AddWithValue("cost", payment.cost);
                        sqlCmd.Parameters.AddWithValue("paid", payment.paid);
                        sqlCmd.Parameters.AddWithValue("paidTime", payment.paidTime);
                        sqlCmd.ExecuteNonQuery();
                    }
                    using (var sqlCmd = new SqlCommand(_sqlQuery.SELECT_LAST_PAYMENT, connection))
                    {
                        Payment inserted = new Payment();
                        using (var dataReader = sqlCmd.ExecuteReader())
                        {
                            while (dataReader.Read())
                            {
                                inserted.paymentID = Convert.ToInt32(dataReader["paymentID"]);
                                inserted.cost = Convert.ToDecimal(dataReader["cost"]);
                                inserted.paid = Convert.ToBoolean(dataReader["paid"]);
                            }
                        }
                        return inserted;
                    }
                }
            }
            catch (Exception)
            {
                throw new BadRequestException();
            }
        }
        public List<Payment> getAllPayments()
        {
            try
            {
                List<Payment> result = new List<Payment>();
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    using (var sqlCmd = new SqlCommand(_sqlQuery.SELECT_ALL_PAYMENT, connection))
                    {
                        sqlCmd.CommandType = CommandType.Text;
                        using (var dataReader = sqlCmd.ExecuteReader())
                        {
                            while (dataReader.Read())
                            {
                                var payment = new Payment
                                {
                                    paymentID = Convert.ToInt32(dataReader["paymentID"]),
                                    cost = Convert.ToDecimal(dataReader["cost"]),
                                    paid = Convert.ToBoolean(dataReader["paid"]),
                                    paidTime = Convert.ToString(dataReader["paidTime"])
                                };
                                result.Add(payment);
                            }
                        }
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw new EntityNotFoundException();
            }
        }
        public void updatePayment(Payment payment)
        {
            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    using (var sqlCmd = new SqlCommand(_sqlQuery.UPDATE_PAYMENT, connection))
                    {
                        connection.Open();
                        sqlCmd.Connection = connection;
                        sqlCmd.Parameters.AddWithValue("id", payment.paymentID);
                        sqlCmd.Parameters.AddWithValue("cost", payment.cost);
                        sqlCmd.Parameters.AddWithValue("paid", payment.paid);
                        sqlCmd.Parameters.AddWithValue("paidTime", payment.paidTime);
                        int rowAffected = sqlCmd.ExecuteNonQuery();
                        if (rowAffected == 0)
                            throw new BadRequestException();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public void deletePayment(int paymentID)
        {
            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    using (var sqlCmd = new SqlCommand(_sqlQuery.DELETE_PAYMENT))
                    {
                        connection.Open();
                        sqlCmd.Connection = connection;
                        sqlCmd.Parameters.AddWithValue("id", paymentID);
                        int rowAffected = sqlCmd.ExecuteNonQuery();
                        if (rowAffected == 0)
                            throw new BadRequestException();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public Payment getPaymentByID(int id)
        {
            try
            {
                Payment payment = null;
                using (var connection = new SqlConnection(_connectionString))
                {
                    SqlCommand sqlCmd = new SqlCommand(_sqlQuery.SELECT_PAYMENT_BY_ID + id, connection)
                    {
                        CommandType = CommandType.Text
                    };
                    connection.Open();
                    using (var dataReader = sqlCmd.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            payment = new Payment
                            {
                                paymentID = Convert.ToInt32(dataReader["paymentID"]),
                                cost = Convert.ToDecimal(dataReader["cost"]),
                                paid = Convert.ToBoolean(dataReader["paid"]),
                                paidTime = Convert.ToString(dataReader["paidTime"])
                            };
                        }
                    }
                    if (payment == null)
                        throw new BadRequestException();
                }
                return payment;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}
