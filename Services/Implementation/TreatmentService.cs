using Timesheet.NETCore.Exceptions;
using Timesheet.NETCore.Models;
using System;
using System.Collections.Generic;
using System.Data;
using Timesheet.NETCore.Consts;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;

namespace Timesheet.NETCore.Services.Implementation
{
    public class TreatmentService : ITreatmentService
    {
        private readonly string _connectionString;
        private SqlQuery _sqlQuery;
        public TreatmentService(IConfiguration Configuration)
        {
            _connectionString = Configuration["ConnectionString:DBCS"];
            _sqlQuery = new SqlQuery();
        }
        public Treatment createTreatment(Treatment treatment)
        {
            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    using (var sqlCmd = new SqlCommand(_sqlQuery.CREATE_TREATMENT, connection))
                    {
                        sqlCmd.Parameters.AddWithValue("doctorID", treatment.doctorID);
                        sqlCmd.Parameters.AddWithValue("patientID", treatment.patientID);
                        sqlCmd.Parameters.AddWithValue("serviceID", treatment.serviceID);
                        sqlCmd.Parameters.AddWithValue("completed", treatment.completed);
                        sqlCmd.Parameters.AddWithValue("scheduledTime", treatment.scheduledTime);
                        sqlCmd.Parameters.AddWithValue("toothNo", treatment.toothNo);
                        sqlCmd.Parameters.AddWithValue("type", treatment.type);
                        sqlCmd.Parameters.AddWithValue("zone", treatment.zone);
                        sqlCmd.ExecuteNonQuery();
                    }
                    using (var sqlCmd = new SqlCommand(_sqlQuery.SELECT_LAST_TREATMENT, connection))
                    {
                        Treatment inserted = new Treatment();
                        using (var dataReader = sqlCmd.ExecuteReader())
                        {
                            while (dataReader.Read())
                            {
                                inserted.treatmentID = Convert.ToInt32(dataReader["treatmentID"]);
                                inserted.doctorID = Convert.ToInt32(dataReader["doctorID"]);
                                inserted.patientID = Convert.ToInt32(dataReader["patientID"]);
                                inserted.serviceID = Convert.ToInt32(dataReader["serviceID"]);
                                inserted.completed = Convert.ToBoolean(dataReader["completed"]);
                                inserted.scheduledTime = Convert.ToString(dataReader["scheduledTime"]);
                                inserted.toothNo = Convert.ToString(dataReader["toothNo"]);
                                inserted.type = Convert.ToString(dataReader["type"]);
                                inserted.zone = Convert.ToString(dataReader["zone"]);
                            }
                        }
                        return inserted;
                    }
                }
            }
            catch (Exception)
            {
                throw new BadRequestException();
            }
        }
        public List<Treatment> getAllTreatments()
        {
            try
            {
                List<Treatment> result = new List<Treatment>();
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    using (var sqlCmd = new SqlCommand(_sqlQuery.SELECT_ALL_TREATMENT, connection))
                    {
                        sqlCmd.CommandType = CommandType.Text;
                        using (var dataReader = sqlCmd.ExecuteReader())
                        {
                            while (dataReader.Read())
                            {
                                var treatment = new Treatment
                                {
                                    treatmentID = Convert.ToInt32(dataReader["treatmentID"]),
                                    doctorID = Convert.ToInt32(dataReader["doctorID"]),
                                    patientID = Convert.ToInt32(dataReader["patientID"]),
                                    serviceID = Convert.ToInt32(dataReader["serviceID"]),
                                    completed = Convert.ToBoolean(dataReader["completed"]),
                                    scheduledTime = Convert.ToString(dataReader["scheduledTime"]),
                                    toothNo = Convert.ToString(dataReader["toothNo"]),
                                    type = Convert.ToString(dataReader["type"]),
                                    zone = Convert.ToString(dataReader["zone"])
                                };
                                result.Add(treatment);
                            }
                        }
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw new EntityNotFoundException();
            }
        }
        public void updateTreatment(Treatment treatment)
        {
            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    using (var sqlCmd = new SqlCommand(_sqlQuery.UPDATE_TREATMENT, connection))
                    {
                        connection.Open();
                        sqlCmd.Connection = connection;
                        sqlCmd.Parameters.AddWithValue("id", treatment.treatmentID);
                        sqlCmd.Parameters.AddWithValue("doctorID", treatment.doctorID);
                        sqlCmd.Parameters.AddWithValue("patientID", treatment.patientID);
                        sqlCmd.Parameters.AddWithValue("serviceID", treatment.serviceID);
                        sqlCmd.Parameters.AddWithValue("completed", treatment.completed);
                        sqlCmd.Parameters.AddWithValue("scheduledTime", treatment.scheduledTime);
                        sqlCmd.Parameters.AddWithValue("toothNo", treatment.toothNo);
                        sqlCmd.Parameters.AddWithValue("type", treatment.type);
                        sqlCmd.Parameters.AddWithValue("zone", treatment.zone);
                        int rowAffected = sqlCmd.ExecuteNonQuery();
                        if (rowAffected == 0)
                            throw new BadRequestException();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void deleteTreatment(int treatmentID)
        {
            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    using (var sqlCmd = new SqlCommand(_sqlQuery.DELETE_TREATMENT))
                    {
                        connection.Open();
                        sqlCmd.Connection = connection;
                        sqlCmd.Parameters.AddWithValue("id", treatmentID);
                        int rowAffected = sqlCmd.ExecuteNonQuery();
                        if (rowAffected == 0)
                            throw new BadRequestException();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public Treatment getTreatmentByID(int id)
        {
            try
            {
                Treatment treatment = null;
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    using (var sqlCmd = new SqlCommand(_sqlQuery.SELECT_TREATMENT_BY_ID, connection))
                    {
                        sqlCmd.CommandType = CommandType.Text;
                        sqlCmd.Parameters.AddWithValue("id", id);
                        using (var dataReader = sqlCmd.ExecuteReader())
                        {
                            while (dataReader.Read())
                            {
                                treatment = new Treatment
                                {
                                    treatmentID = Convert.ToInt32(dataReader["treatmentID"]),
                                    doctorID = Convert.ToInt32(dataReader["doctorID"]),
                                    patientID = Convert.ToInt32(dataReader["patientID"]),
                                    serviceID = Convert.ToInt32(dataReader["serviceID"]),
                                    completed = Convert.ToBoolean(dataReader["completed"]),
                                    scheduledTime = Convert.ToString(dataReader["scheduledTime"]),
                                    toothNo = Convert.ToString(dataReader["toothNo"]),
                                    type = Convert.ToString(dataReader["type"]),
                                    zone = Convert.ToString(dataReader["zone"])
                                };
                            }
                        };
                        if (treatment == null)
                            throw new BadRequestException();
                    }
                }
                return treatment;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<TreatmentExtend> getTreatmentsExtendByPatient(int patientID)
        {
            try
            {
                List<TreatmentExtend> result = new List<TreatmentExtend>();
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    using (var sqlCmd = new SqlCommand(_sqlQuery.SELECT_ALL_TREATMENT_EXTEND_BY_PATIENTID, connection))
                    {
                        sqlCmd.CommandType = CommandType.Text;
                        sqlCmd.Parameters.AddWithValue("patientID", patientID);
                        using (var dataReader = sqlCmd.ExecuteReader())
                        {
                            while (dataReader.Read())
                            {
                                var treatment = new TreatmentExtend
                                {
                                    treatmentID = Convert.ToInt32(dataReader["treatmentID"]),
                                    doctorID = Convert.ToInt32(dataReader["doctorID"]),
                                    patientID = Convert.ToInt32(dataReader["patientID"]),
                                    serviceID = Convert.ToInt32(dataReader["serviceID"]),
                                    completed = Convert.ToBoolean(dataReader["completed"]),
                                    scheduledTime = Convert.ToString(dataReader["scheduledTime"]),
                                    toothNo = Convert.ToString(dataReader["toothNo"]),
                                    type = Convert.ToString(dataReader["type"]),
                                    zone = Convert.ToString(dataReader["zone"]),
                                    title = Convert.ToString(dataReader["title"]),
                                    doctorFullName = Convert.ToString(dataReader["doctorFullName"]),
                                    procedure = Convert.ToString(dataReader["procedure"]),
                                    cost = Convert.ToInt32(dataReader["cost"]),
                                    duration = Convert.ToInt32(dataReader["duration"])
                                };
                                result.Add(treatment);
                            }
                        }
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw new EntityNotFoundException();
            }
        }
        public List<TreatmentExtend> getTreatmentsExtendByDoctor(int doctorID)
        {
            try
            {
                List<TreatmentExtend> result = new List<TreatmentExtend>();
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    using (var sqlCmd = new SqlCommand(_sqlQuery.SELECT_ALL_TREATMENT_EXTEND_BY_DOCTORID, connection))
                    {
                        sqlCmd.CommandType = CommandType.Text;
                        sqlCmd.Parameters.AddWithValue("doctorID", doctorID);
                        using (var dataReader = sqlCmd.ExecuteReader())
                        {
                            while (dataReader.Read())
                            {
                                var treatment = new TreatmentExtend
                                {
                                    treatmentID = Convert.ToInt32(dataReader["treatmentID"]),
                                    doctorID = Convert.ToInt32(dataReader["doctorID"]),
                                    patientID = Convert.ToInt32(dataReader["patientID"]),
                                    serviceID = Convert.ToInt32(dataReader["serviceID"]),
                                    completed = Convert.ToBoolean(dataReader["completed"]),
                                    scheduledTime = Convert.ToString(dataReader["scheduledTime"]),
                                    toothNo = Convert.ToString(dataReader["toothNo"]),
                                    type = Convert.ToString(dataReader["type"]),
                                    zone = Convert.ToString(dataReader["zone"]),
                                    title = Convert.ToString(dataReader["title"]),
                                    doctorFullName = Convert.ToString(dataReader["doctorFullName"]),
                                    procedure = Convert.ToString(dataReader["procedure"]),
                                    cost = Convert.ToInt32(dataReader["cost"]),
                                    duration = Convert.ToInt32(dataReader["duration"])
                                };
                                result.Add(treatment);
                            }
                        }
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw new EntityNotFoundException();
            }
        }
        public List<TreatmentExtend> getAllTreatmentsExtend()
        {
            try
            {
                List<TreatmentExtend> result = new List<TreatmentExtend>();
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    using (var sqlCmd = new SqlCommand(_sqlQuery.SELECT_ALL_TREATMENT_EXTEND, connection))
                    {
                        sqlCmd.CommandType = CommandType.Text;
                        using (var dataReader = sqlCmd.ExecuteReader())
                        {
                            while (dataReader.Read())
                            {
                                var treatment = new TreatmentExtend
                                {
                                    treatmentID = Convert.ToInt32(dataReader["treatmentID"]),
                                    doctorID = Convert.ToInt32(dataReader["doctorID"]),
                                    patientID = Convert.ToInt32(dataReader["patientID"]),
                                    serviceID = Convert.ToInt32(dataReader["serviceID"]),
                                    completed = Convert.ToBoolean(dataReader["completed"]),
                                    scheduledTime = Convert.ToString(dataReader["scheduledTime"]),
                                    toothNo = Convert.ToString(dataReader["toothNo"]),
                                    type = Convert.ToString(dataReader["type"]),
                                    zone = Convert.ToString(dataReader["zone"]),
                                    title = Convert.ToString(dataReader["title"]),
                                    doctorFullName = Convert.ToString(dataReader["doctorFullName"]),
                                    procedure = Convert.ToString(dataReader["procedure"]),
                                    cost = Convert.ToInt32(dataReader["cost"]),
                                    duration = Convert.ToInt32(dataReader["duration"])
                                };
                                result.Add(treatment);
                            }
                        }
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw new EntityNotFoundException();
            }
        }
    }
}
