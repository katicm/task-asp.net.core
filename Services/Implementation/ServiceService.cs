using Timesheet.NETCore.Exceptions;
using Timesheet.NETCore.Models;
using System;
using System.Collections.Generic;
using System.Data;
using Timesheet.NETCore.Consts;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;

namespace Timesheet.NETCore.Services.Implementation
{
    public class ServiceService : IServiceService
    {
        private readonly string _connectionString;
        private SqlQuery _sqlQuery;
        public ServiceService(IConfiguration Configuration)
        {
            _connectionString = Configuration["ConnectionString:DBCS"];
            _sqlQuery = new SqlQuery();
        }
        public Service createService(Service service)
        {
            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    using (var sqlCmd = new SqlCommand(_sqlQuery.CREATE_SERVICE, connection))
                    {
                        sqlCmd.Parameters.AddWithValue("name", service.name);
                        sqlCmd.Parameters.AddWithValue("cost", service.cost);
                        sqlCmd.Parameters.AddWithValue("duration", service.duration);
                        sqlCmd.ExecuteNonQuery();
                    }
                    using (var sqlCmd = new SqlCommand(_sqlQuery.SELECT_LAST_SERVICE, connection))
                    {
                        Service inserted = new Service();
                        using (var dataReader = sqlCmd.ExecuteReader())
                        {
                            while (dataReader.Read())
                            {
                                inserted.name = Convert.ToString(dataReader["name"]);
                                inserted.cost = Convert.ToInt32(dataReader["cost"]);
                                inserted.duration = Convert.ToInt32(dataReader["duration"]);
                            }
                        }
                        return inserted;
                    }
                }
            }
            catch (Exception)
            {
                throw new BadRequestException();
            }
        }
        public List<Service> getAllServices()
        {
            try
            {
                List<Service> result = new List<Service>();
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    using (var sqlCmd = new SqlCommand(_sqlQuery.SELECT_ALL_SERVICE, connection))
                    {
                        sqlCmd.CommandType = CommandType.Text;
                        using (var dataReader = sqlCmd.ExecuteReader())
                        {
                            while (dataReader.Read())
                            {
                                var service = new Service
                                {
                                    serviceID = Convert.ToInt32(dataReader["serviceID"]),
                                    name = Convert.ToString(dataReader["name"]),
                                    cost = Convert.ToInt32(dataReader["cost"]),
                                    duration = Convert.ToInt32(dataReader["duration"])
                                };
                                result.Add(service);
                            }
                        }
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw new EntityNotFoundException();
            }
        }
        public void updateService(Service service)
        {
            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    using (var sqlCmd = new SqlCommand(_sqlQuery.UPDATE_SERVICE, connection))
                    {
                        connection.Open();
                        sqlCmd.Connection = connection;
                        sqlCmd.Parameters.AddWithValue("id", service.serviceID);
                        sqlCmd.Parameters.AddWithValue("name", service.name);
                        sqlCmd.Parameters.AddWithValue("cost", service.cost);
                        sqlCmd.Parameters.AddWithValue("duration", service.duration);
                        int rowAffected = sqlCmd.ExecuteNonQuery();
                        if (rowAffected == 0)
                            throw new BadRequestException();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void deleteService(int serviceID)
        {
            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    using (var sqlCmd = new SqlCommand(_sqlQuery.DELETE_SERVICE))
                    {
                        connection.Open();
                        sqlCmd.Connection = connection;
                        sqlCmd.Parameters.AddWithValue("id", serviceID);
                        int rowAffected = sqlCmd.ExecuteNonQuery();
                        if (rowAffected == 0)
                            throw new BadRequestException();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public Service getServiceByID(int id)
        {
            try
            {
                Service service = null;
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    using (var sqlCmd = new SqlCommand(_sqlQuery.SELECT_SERVICE_BY_ID, connection))
                    {
                        sqlCmd.CommandType = CommandType.Text;
                        sqlCmd.Parameters.AddWithValue("id", id);
                        using (var dataReader = sqlCmd.ExecuteReader())
                        {
                            while (dataReader.Read())
                            {
                                service = new Service
                                {
                                    serviceID = Convert.ToInt32(dataReader["serviceID"]),
                                    name = Convert.ToString(dataReader["name"]),
                                    cost = Convert.ToInt32(dataReader["cost"]),
                                    duration = Convert.ToInt32(dataReader["duration"])
                                };
                            }
                        };
                        if (service == null)
                            throw new BadRequestException();
                    }
                }
                return service;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
