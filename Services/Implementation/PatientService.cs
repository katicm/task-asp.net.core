using Timesheet.NETCore.Exceptions;
using Timesheet.NETCore.Models;
using System;
using System.Collections.Generic;
using System.Data;
using Timesheet.NETCore.Consts;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;

namespace Timesheet.NETCore.Services.Implementation
{
    public class PatientService : IPatientService
    {
        private readonly string _connectionString;
        private SqlQuery _sqlQuery;
        public PatientService(IConfiguration Configuration)
        {
            _connectionString = Configuration["ConnectionString:DBCS"];
            _sqlQuery = new SqlQuery();
        }
        public Patient createPatient(Patient patient)
        {
            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    using (var sqlCmd = new SqlCommand(_sqlQuery.CREATE_PATIENT, connection))
                    {
                        sqlCmd.Parameters.AddWithValue("userID", patient.userID);
                        sqlCmd.Parameters.AddWithValue("bloodGroup", patient.bloodGroup);
                        sqlCmd.Parameters.AddWithValue("notes", patient.notes);
                        sqlCmd.Parameters.AddWithValue("firstVisit", patient.firstVisit);
                        sqlCmd.Parameters.AddWithValue("lastVisit", patient.lastVisit);
                        sqlCmd.Parameters.AddWithValue("allergy", patient.allergy);
                        sqlCmd.Parameters.AddWithValue("visitCount", patient.visitCount);
                        sqlCmd.ExecuteNonQuery();
                    }
                    using (var sqlCmd = new SqlCommand(_sqlQuery.SELECT_LAST_PATIENT, connection))
                    {
                        Patient inserted = new Patient();
                        using (var dataReader = sqlCmd.ExecuteReader())
                        {
                            while (dataReader.Read())
                            {
                                inserted.patientID = Convert.ToInt32(dataReader["patientID"]);
                                inserted.userID = Convert.ToInt32(dataReader["userID"]);
                                inserted.bloodGroup = Convert.ToString(dataReader["bloodGroup"]);
                                inserted.notes = Convert.ToString(dataReader["notes"]);
                                inserted.firstVisit = Convert.ToString(dataReader["firstVisit"]);
                                inserted.lastVisit = Convert.ToString(dataReader["lastVisit"]);
                                inserted.allergy = Convert.ToString(dataReader["allergy"]);
                                inserted.visitCount = Convert.ToInt32(dataReader["visitCount"]);
                            }
                        }
                        return inserted;
                    }
                }
            }
            catch (Exception)
            {
                throw new BadRequestException();
            }
        }
        public List<Patient> getAllPatients()
        {
            try
            {
                List<Patient> result = new List<Patient>();
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    using (var sqlCmd = new SqlCommand(_sqlQuery.SELECT_ALL_PATIENT, connection))
                    {
                        sqlCmd.CommandType = CommandType.Text;
                        using (var dataReader = sqlCmd.ExecuteReader())
                        {
                            while (dataReader.Read())
                            {
                                var patient = new Patient
                                {
                                    patientID = Convert.ToInt32(dataReader["patientID"]),
                                    userID = Convert.ToInt32(dataReader["userID"]),
                                    bloodGroup = Convert.ToString(dataReader["bloodGroup"]),
                                    notes = Convert.ToString(dataReader["notes"]),
                                    firstVisit = Convert.ToString(dataReader["firstVisit"]),
                                    lastVisit = Convert.ToString(dataReader["lastVisit"]),
                                    allergy = Convert.ToString(dataReader["allergy"]),
                                    visitCount = Convert.ToInt32(dataReader["visitCount"])
                                };
                                result.Add(patient);
                            }
                        }
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw new EntityNotFoundException();
            }
        }
        public void updatePatient(Patient patient)
        {
            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    using (var sqlCmd = new SqlCommand(_sqlQuery.UPDATE_PATIENT, connection))
                    {
                        connection.Open();
                        sqlCmd.Connection = connection;
                        sqlCmd.Parameters.AddWithValue("id", patient.patientID);
                        sqlCmd.Parameters.AddWithValue("userID", patient.userID);
                        sqlCmd.Parameters.AddWithValue("bloodGroup", patient.bloodGroup);
                        sqlCmd.Parameters.AddWithValue("notes", patient.notes);
                        sqlCmd.Parameters.AddWithValue("firstVisit", patient.firstVisit);
                        sqlCmd.Parameters.AddWithValue("lastVisit", patient.lastVisit);
                        sqlCmd.Parameters.AddWithValue("allergy", patient.allergy);
                        sqlCmd.Parameters.AddWithValue("visitCount", patient.visitCount);
                        int rowAffected = sqlCmd.ExecuteNonQuery();
                        if (rowAffected == 0)
                            throw new BadRequestException();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void deletePatient(int patientID)
        {
            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    using (var sqlCmd = new SqlCommand(_sqlQuery.DELETE_PATIENT))
                    {
                        connection.Open();
                        sqlCmd.Connection = connection;
                        sqlCmd.Parameters.AddWithValue("id", patientID);
                        int rowAffected = sqlCmd.ExecuteNonQuery();
                        if (rowAffected == 0)
                            throw new BadRequestException();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public Patient getPatientByID(int id)
        {
            try
            {
                Patient patient = null;
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    using (var sqlCmd = new SqlCommand(_sqlQuery.SELECT_PATIENT_BY_ID, connection))
                    {
                        sqlCmd.CommandType = CommandType.Text;
                        sqlCmd.Parameters.AddWithValue("id", id);
                        using (var dataReader = sqlCmd.ExecuteReader())
                        {
                            while (dataReader.Read())
                            {
                                patient = new Patient
                                {
                                    patientID = Convert.ToInt32(dataReader["patientID"]),
                                    userID = Convert.ToInt32(dataReader["userID"]),
                                    bloodGroup = Convert.ToString(dataReader["bloodGroup"]),
                                    notes = Convert.ToString(dataReader["notes"]),
                                    firstVisit = Convert.ToString(dataReader["firstVisit"]),
                                    lastVisit = Convert.ToString(dataReader["lastVisit"]),
                                    allergy = Convert.ToString(dataReader["allergy"]),
                                    visitCount = Convert.ToInt32(dataReader["visitCount"]),
                                };
                            }
                        };
                        if (patient == null)
                            throw new BadRequestException();
                    }
                }
                return patient;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<PatientExtend> getAllPatientsExtend()
        {
            try
            {
                List<PatientExtend> result = new List<PatientExtend>();
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    using (var sqlCmd = new SqlCommand(_sqlQuery.SELECT_ALL_PATIENT_EXTEND, connection))
                    {
                        sqlCmd.CommandType = CommandType.Text;
                        using (var dataReader = sqlCmd.ExecuteReader())
                        {
                            while (dataReader.Read())
                            {
                                var patient = new PatientExtend
                                {
                                    patientID = Convert.ToInt32(dataReader["patientID"]),
                                    userID = Convert.ToInt32(dataReader["userID"]),
                                    bloodGroup = Convert.ToString(dataReader["bloodGroup"]),
                                    notes = Convert.ToString(dataReader["notes"]),
                                    firstVisit = Convert.ToString(dataReader["firstVisit"]),
                                    lastVisit = Convert.ToString(dataReader["lastVisit"]),
                                    allergy = Convert.ToString(dataReader["allergy"]),
                                    visitCount = Convert.ToInt32(dataReader["visitCount"]),
                                    fullPatientName = Convert.ToString(dataReader["fullPatientName"]),
                                    middleName = Convert.ToString(dataReader["middleName"]),
                                    gender = Convert.ToString(dataReader["gender"]),
                                    birthDate = Convert.ToString(dataReader["birthDate"]),
                                    address = Convert.ToString(dataReader["address"]),
                                    email = Convert.ToString(dataReader["email"]),
                                    created = Convert.ToString(dataReader["created"]),
                                    phone = Convert.ToString(dataReader["phone"])
                                };
                                result.Add(patient);
                            }
                        }
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw new EntityNotFoundException();
            }
        }
    }
}
