using Timesheet.NETCore.Exceptions;
using Timesheet.NETCore.Models;
using System;
using System.Collections.Generic;
using System.Data;
using Timesheet.NETCore.Consts;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;

namespace Timesheet.NETCore.Services.Implementation
{
    public class VisitService : IVisitService
    {
        private readonly string _connectionString;
        private SqlQuery _sqlQuery;
        public VisitService(IConfiguration Configuration)
        {
            _connectionString = Configuration["ConnectionString:DBCS"];
            _sqlQuery = new SqlQuery();
        }
        public Visit createVisit(Visit visit)
        {
            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    using (var sqlCmd = new SqlCommand(_sqlQuery.CREATE_VISIT, connection))
                    {
                        sqlCmd.Parameters.AddWithValue("appointmentID", visit.appointmentID);
                        sqlCmd.Parameters.AddWithValue("patientID", visit.patientID);
                        sqlCmd.Parameters.AddWithValue("doctorID", visit.doctorID);
                        sqlCmd.Parameters.AddWithValue("paymentID", visit.paymentID);
                        sqlCmd.Parameters.AddWithValue("datetime", visit.datetime);
                        sqlCmd.Parameters.AddWithValue("remark", visit.remark);
                        sqlCmd.ExecuteNonQuery();
                    }
                    using (var sqlCmd = new SqlCommand(_sqlQuery.SELECT_LAST_VISIT, connection))
                    {
                        Visit inserted = new Visit();
                        using (var dataReader = sqlCmd.ExecuteReader())
                        {
                            while (dataReader.Read())
                            {
                                var payment = dataReader["paymentID"];
                                inserted.visitID = Convert.ToInt32(dataReader["visitID"]);
                                inserted.appointmentID = Convert.ToInt32(dataReader["appointmentID"]);
                                inserted.patientID = Convert.ToInt32(dataReader["patientID"]);
                                inserted.doctorID = Convert.ToInt32(dataReader["doctorID"]);
                                inserted.paymentID = payment != DBNull.Value ? Convert.ToInt32(payment) : 0;
                                inserted.datetime = Convert.ToString(dataReader["datetime"]);
                                inserted.remark = Convert.ToString(dataReader["remark"]);
                            }
                        }
                        return inserted;
                    }
                }
            }
            catch (Exception)
            {
                throw new BadRequestException();
            }
        }
        public List<Visit> getAllVisits()
        {
            try
            {
                List<Visit> result = new List<Visit>();
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    using (var sqlCmd = new SqlCommand(_sqlQuery.SELECT_ALL_VISIT, connection))
                    {
                        sqlCmd.CommandType = CommandType.Text;
                        using (var dataReader = sqlCmd.ExecuteReader())
                        {
                            while (dataReader.Read())
                            {
                                var payment = dataReader["paymentID"];
                                var visit = new Visit
                                {
                                    visitID = Convert.ToInt32(dataReader["visitID"]),
                                    appointmentID = Convert.ToInt32(dataReader["appointmentID"]),
                                    patientID = Convert.ToInt32(dataReader["patientID"]),
                                    doctorID = Convert.ToInt32(dataReader["doctorID"]),
                                    paymentID = payment != DBNull.Value ? Convert.ToInt32(payment) : 0,
                                    datetime = Convert.ToString(dataReader["datetime"]),
                                    remark = Convert.ToString(dataReader["remark"])
                                };
                                result.Add(visit);
                            }
                        }
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw new EntityNotFoundException();
            }
        }
        public void updateVisit(Visit visit)
        {
            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    using (var sqlCmd = new SqlCommand(_sqlQuery.UPDATE_VISIT, connection))
                    {
                        connection.Open();
                        sqlCmd.Connection = connection;
                        sqlCmd.Parameters.AddWithValue("id", visit.visitID);
                        sqlCmd.Parameters.AddWithValue("appointmentID", visit.appointmentID);
                        sqlCmd.Parameters.AddWithValue("patientID", visit.patientID);
                        sqlCmd.Parameters.AddWithValue("doctorID", visit.doctorID);
                        sqlCmd.Parameters.AddWithValue("paymentID", visit.paymentID);
                        sqlCmd.Parameters.AddWithValue("datetime", visit.datetime);
                        sqlCmd.Parameters.AddWithValue("remark", visit.remark);
                        int rowAffected = sqlCmd.ExecuteNonQuery();
                        if (rowAffected == 0)
                            throw new BadRequestException();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void deleteVisit(int visitID)
        {
            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    using (var sqlCmd = new SqlCommand(_sqlQuery.DELETE_VISIT))
                    {
                        connection.Open();
                        sqlCmd.Connection = connection;
                        sqlCmd.Parameters.AddWithValue("id", visitID);
                        int rowAffected = sqlCmd.ExecuteNonQuery();
                        if (rowAffected == 0)
                            throw new BadRequestException();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public Visit getVisitByID(int id)
        {
            try
            {
                Visit visit = null;
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    using (var sqlCmd = new SqlCommand(_sqlQuery.SELECT_VISIT_BY_ID, connection))
                    {
                        sqlCmd.CommandType = CommandType.Text;
                        sqlCmd.Parameters.AddWithValue("id", id);
                        using (var dataReader = sqlCmd.ExecuteReader())
                        {
                            while (dataReader.Read())
                            {
                                var payment = dataReader["paymentID"];
                                visit = new Visit
                                {
                                    visitID = Convert.ToInt32(dataReader["visitID"]),
                                    appointmentID = Convert.ToInt32(dataReader["appointmentID"]),
                                    patientID = Convert.ToInt32(dataReader["patientID"]),
                                    doctorID = Convert.ToInt32(dataReader["doctorID"]),
                                    paymentID = payment != DBNull.Value ? Convert.ToInt32(payment) : 0,
                                    datetime = Convert.ToString(dataReader["datetime"]),
                                    remark = Convert.ToString(dataReader["remark"])
                                };
                            }
                        };
                        if (visit == null)
                            throw new BadRequestException();
                    }
                }
                return visit;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
