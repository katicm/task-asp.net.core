using Timesheet.NETCore.Exceptions;
using Timesheet.NETCore.Models;
using System;
using System.Collections.Generic;
using System.Data;
using Timesheet.NETCore.Consts;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;

namespace Timesheet.NETCore.Services.Implementation
{
    public class DoctorService : IDoctorService
    {
        private readonly string _connectionString;
        private SqlQuery _sqlQuery;
        public DoctorService(IConfiguration Configuration)
        {
            _connectionString = Configuration["ConnectionString:DBCS"];
            _sqlQuery = new SqlQuery();
        }
        public Doctor createDoctor(Doctor doctor)
        {
            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    using (var sqlCmd = new SqlCommand(_sqlQuery.CREATE_DOCTOR, connection))
                    {
                        sqlCmd.Parameters.AddWithValue("userID", doctor.userID);
                        sqlCmd.Parameters.AddWithValue("departmentID", doctor.departmentID);
                        sqlCmd.Parameters.AddWithValue("title", doctor.title);
                        sqlCmd.ExecuteNonQuery();
                    }
                    using (var sqlCmd = new SqlCommand(_sqlQuery.SELECT_LAST_DOCTOR, connection))
                    {
                        Doctor inserted = new Doctor();
                        using (var dataReader = sqlCmd.ExecuteReader())
                        {
                            while (dataReader.Read())
                            {
                                inserted.doctorID = Convert.ToInt32(dataReader["doctorID"]);
                                inserted.userID = Convert.ToInt32(dataReader["userID"]);
                                inserted.departmentID = Convert.ToInt32(dataReader["departmentID"]);
                                inserted.title = Convert.ToString(dataReader["title"]);
                            }
                        }
                        return inserted;
                    }
                }
            }
            catch (Exception)
            {
                throw new BadRequestException();
            }
        }
        public List<Doctor> getAllDoctors()
        {
            try
            {
                List<Doctor> result = new List<Doctor>();
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    using (var sqlCmd = new SqlCommand(_sqlQuery.SELECT_ALL_DOCTOR, connection))
                    {
                        sqlCmd.CommandType = CommandType.Text;
                        using (var dataReader = sqlCmd.ExecuteReader())
                        {
                            while (dataReader.Read())
                            {
                                var doctor = new Doctor
                                {
                                    doctorID = Convert.ToInt32(dataReader["doctorID"]),
                                    userID = Convert.ToInt32(dataReader["userID"]),
                                    departmentID = Convert.ToInt32(dataReader["departmentID"]),
                                    title = Convert.ToString(dataReader["title"])
                                };
                                result.Add(doctor);
                            }
                        }
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw new EntityNotFoundException();
            }
        }
        public void updateDoctor(Doctor doctor)
        {
            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    using (var sqlCmd = new SqlCommand(_sqlQuery.UPDATE_DOCTOR, connection))
                    {
                        connection.Open();
                        sqlCmd.Connection = connection;
                        sqlCmd.Parameters.AddWithValue("id", doctor.doctorID);
                        sqlCmd.Parameters.AddWithValue("userID", doctor.userID);
                        sqlCmd.Parameters.AddWithValue("departmentID", doctor.departmentID);
                        sqlCmd.Parameters.AddWithValue("title", doctor.title);
                        int rowAffected = sqlCmd.ExecuteNonQuery();
                        if (rowAffected == 0)
                            throw new BadRequestException();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void deleteDoctor(int doctorID)
        {
            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    using (var sqlCmd = new SqlCommand(_sqlQuery.DELETE_DOCTOR))
                    {
                        connection.Open();
                        sqlCmd.Connection = connection;
                        sqlCmd.Parameters.AddWithValue("id", doctorID);
                        int rowAffected = sqlCmd.ExecuteNonQuery();
                        if (rowAffected == 0)
                            throw new BadRequestException();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public Doctor getDoctorByID(int id)
        {
            try
            {
                Doctor doctor = null;
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    using (var sqlCmd = new SqlCommand(_sqlQuery.SELECT_DOCTOR_BY_ID, connection))
                    {
                        sqlCmd.CommandType = CommandType.Text;
                        sqlCmd.Parameters.AddWithValue("id", id);
                        using (var dataReader = sqlCmd.ExecuteReader())
                        {
                            while (dataReader.Read())
                            {
                                doctor = new Doctor
                                {
                                    doctorID = Convert.ToInt32(dataReader["doctorID"]),
                                    userID = Convert.ToInt32(dataReader["userID"]),
                                    departmentID = Convert.ToInt32(dataReader["departmentID"]),
                                    title = Convert.ToString(dataReader["title"])
                                };
                            }
                        };
                        if (doctor == null)
                            throw new BadRequestException();
                    }
                }
                return doctor;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
