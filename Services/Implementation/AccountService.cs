using Timesheet.NETCore.Exceptions;
using Timesheet.NETCore.Models;
using System;
using System.Collections.Generic;
using System.Data;
using Timesheet.NETCore.Consts;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;

namespace Timesheet.NETCore.Services.Implementation
{
    public class AccountService : IAccountService
    {
        private readonly string _connectionString;
        private SqlQuery _sqlQuery;
        public AccountService(IConfiguration Configuration)
        {
            _connectionString = Configuration["ConnectionString:DBCS"];
            _sqlQuery = new SqlQuery();
        }
        public Account createAccount(Account account)
        {
            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    using (var sqlCmd = new SqlCommand(_sqlQuery.CREATE_ACCOUNT(), connection))
                    {
                        sqlCmd.Parameters.AddWithValue("email", account.email);
                        sqlCmd.Parameters.AddWithValue("username", account.username);
                        sqlCmd.Parameters.AddWithValue("role", account.role);
                        sqlCmd.Parameters.AddWithValue("password", account.password);
                        sqlCmd.Parameters.AddWithValue("phone", account.phone);
                        sqlCmd.ExecuteNonQuery();
                    }
                    using (var sqlCmd = new SqlCommand(_sqlQuery.SELECT_LAST_ACCOUNT(), connection))
                    {
                        Account inserted = new Account();
                        using (var dataReader = sqlCmd.ExecuteReader())
                        {
                            while (dataReader.Read())
                            {
                                inserted.accountID = Convert.ToInt32(dataReader["accountID"]);
                                inserted.email = Convert.ToString(dataReader["email"]);
                                inserted.username = Convert.ToString(dataReader["username"]);
                                inserted.password = Convert.ToString(dataReader["password"]);
                                inserted.created = Convert.ToString(dataReader["created"]);
                                inserted.phone = Convert.ToString(dataReader["phone"]);
                            }
                        }
                        return inserted;
                    }
                }
            }
            catch (Exception)
            {
                throw new BadRequestException();
            }
        }
        public List<Account> getAllAccounts()
        {
            try
            {
                List<Account> result = new List<Account>();
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    using (var sqlCmd = new SqlCommand(_sqlQuery.SELECT_ALL_ACCOUNTS(), connection))
                    {
                        sqlCmd.CommandType = CommandType.Text;
                        using (var dataReader = sqlCmd.ExecuteReader())
                        {
                            while (dataReader.Read())
                            {
                                var account = new Account
                                {
                                    accountID = Convert.ToInt32(dataReader["accountID"]),
                                    email = Convert.ToString(dataReader["email"]),
                                    username = Convert.ToString(dataReader["username"]),
                                    role = Convert.ToString(dataReader["role"]),
                                    password = Convert.ToString(dataReader["password"]),
                                    created = Convert.ToString(dataReader["created"]),
                                    phone = Convert.ToString(dataReader["phone"])
                                };
                                result.Add(account);
                            }
                        }
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw new EntityNotFoundException();
            }
        }
        public void updateAccount(Account account)
        {
            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    using (var sqlCmd = new SqlCommand(_sqlQuery.UPDATE_ACCOUNT(), connection))
                    {
                        connection.Open();
                        sqlCmd.Connection = connection;
                        sqlCmd.Parameters.AddWithValue("id", account.accountID);
                        sqlCmd.Parameters.AddWithValue("email", account.email);
                        sqlCmd.Parameters.AddWithValue("username", account.username);
                        sqlCmd.Parameters.AddWithValue("role", account.role);
                        sqlCmd.Parameters.AddWithValue("password", account.password);
                        sqlCmd.Parameters.AddWithValue("phone", account.phone);
                        int rowAffected = sqlCmd.ExecuteNonQuery();
                        if (rowAffected == 0)
                            throw new BadRequestException();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public void deleteAccount(int accountID)
        {
            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    using (var sqlCmd = new SqlCommand(_sqlQuery.DELETE_ACCOUNT()))
                    {
                        connection.Open();
                        sqlCmd.Connection = connection;
                        sqlCmd.Parameters.AddWithValue("id", accountID);
                        int rowAffected = sqlCmd.ExecuteNonQuery();
                        if (rowAffected == 0)
                            throw new BadRequestException();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public Account getAccountByID(int id)
        {
            try
            {
                Account account = null;
                using (var connection = new SqlConnection(_connectionString))
                {
                    SqlCommand sqlCmd = new SqlCommand(_sqlQuery.SELECT_ACCOUNT_BY_ID() + id, connection)
                    {
                        CommandType = CommandType.Text
                    };
                    connection.Open();
                    using (var dataReader = sqlCmd.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            account = new Account
                            {
                                accountID = Convert.ToInt32(dataReader["accountID"]),
                                email = Convert.ToString(dataReader["email"]),
                                username = Convert.ToString(dataReader["username"]),
                                role = Convert.ToString(dataReader["role"]),
                                password = Convert.ToString(dataReader["password"]),
                                created = Convert.ToString(dataReader["created"]),
                                phone = Convert.ToString(dataReader["phone"])
                            };
                        }
                    }
                    if (account == null)
                        throw new BadRequestException();
                }
                return account;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public Account login(string email, string password)
        {
            try
            {
                Account login = null;
                using (var connection = new SqlConnection(_connectionString))
                {
                    using (var sqlCmd = new SqlCommand(_sqlQuery.LOGIN(), connection))
                    {
                        connection.Open();
                        sqlCmd.Connection = connection;
                        sqlCmd.Parameters.AddWithValue("email", email);
                        sqlCmd.Parameters.AddWithValue("password", password);
                        using (var dataReader = sqlCmd.ExecuteReader())
                        {
                            while (dataReader.Read())
                            {
                                login = new Account
                                {
                                    accountID = Convert.ToInt32(dataReader["accountID"]),
                                    email = Convert.ToString(dataReader["email"]),
                                    username = Convert.ToString(dataReader["username"]),
                                    password = Convert.ToString(dataReader["password"]),
                                    created = Convert.ToString(dataReader["created"]),
                                    phone = Convert.ToString(dataReader["phone"])
                                };
                            }
                            if (login == null)
                                throw new BadRequestException();
                        }
                        return login;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}
