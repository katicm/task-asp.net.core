using Timesheet.NETCore.Exceptions;
using Timesheet.NETCore.Models;
using System;
using System.Collections.Generic;
using System.Data;
using Timesheet.NETCore.Consts;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;

namespace Timesheet.NETCore.Services.Implementation
{
    public class TaskService : ITaskService
    {
        private readonly string _connectionString;
        private SqlQuery _sqlQuery;
        public TaskService(IConfiguration Configuration)
        {
            _connectionString = Configuration["ConnectionString:DBCS"];
            _sqlQuery = new SqlQuery();
        }

        public TaskModel createTask(TaskModel task)
        {
            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    using (var sqlCmd = new SqlCommand(_sqlQuery.CREATE_TASK(), connection))
                    {
                        sqlCmd.Parameters.AddWithValue("title", task.title);
                        sqlCmd.Parameters.AddWithValue("hours", task.hours);
                        sqlCmd.Parameters.AddWithValue("date", task.date);
                        sqlCmd.ExecuteNonQuery();
                    }
                    using (var sqlCmd = new SqlCommand(_sqlQuery.SELECT_LAST_TASK(), connection))
                    {
                        TaskModel inserted = new TaskModel();
                        using (var dataReader = sqlCmd.ExecuteReader())
                        {
                            while (dataReader.Read())
                            {
                                inserted.taskID = Convert.ToInt32(dataReader["taskID"]);
                                inserted.title = Convert.ToString(dataReader["title"]);
                                inserted.hours = Convert.ToInt32(dataReader["hours"]);
                                inserted.date = Convert.ToString(dataReader["date"]);
                            }
                        }
                        return inserted;
                    }
                }
            }
            catch (Exception)
            {
                throw new BadRequestException();
            }

        }
        public List<TaskModel> GetTasksByDate(string date)
        {
            try
            {
                List<TaskModel> result = new List<TaskModel>();
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    using (var sqlCmd = new SqlCommand(_sqlQuery.SELECT_TASK_BY_DATE(date), connection))
                    {
                        sqlCmd.CommandType = CommandType.Text;
                        using (var dataReader = sqlCmd.ExecuteReader())
                        {
                            while (dataReader.Read())
                            {
                                var task = new TaskModel
                                {
                                    taskID = Convert.ToInt32(dataReader["taskID"]),
                                    title = Convert.ToString(dataReader["title"]),
                                    hours = Convert.ToInt32(dataReader["hours"]),
                                    date = Convert.ToString(dataReader["date"]),
                                };
                                result.Add(task);
                            }
                        }
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw new EntityNotFoundException();
            }
        }

        public List<TaskModel> GetAllTasks()
        {
            try
            {
                List<TaskModel> result = new List<TaskModel>();
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    using (var sqlCmd = new SqlCommand(_sqlQuery.SELECT_ALL_TASKS(), connection))
                    {
                        sqlCmd.CommandType = CommandType.Text;
                        using (var dataReader = sqlCmd.ExecuteReader())
                        {
                            while (dataReader.Read())
                            {
                                var task = new TaskModel
                                {
                                    taskID = Convert.ToInt32(dataReader["taskID"]),
                                    title = Convert.ToString(dataReader["title"]),
                                    hours = Convert.ToInt32(dataReader["hours"]),
                                    date = Convert.ToString(dataReader["date"]),
                                };
                                result.Add(task);
                            }
                        }
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw new EntityNotFoundException();
            }
        }
        public TaskModel GetTaskByID(int taskID)
        {
            try
            {
                TaskModel task = null;
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    using (var sqlCmd = new SqlCommand(_sqlQuery.SELECT_TASK_BY_ID(taskID), connection))
                    {
                        sqlCmd.CommandType = CommandType.Text;
                        using (var dataReader = sqlCmd.ExecuteReader())
                        {
                            while (dataReader.Read())
                            {
                                task = new TaskModel
                                {
                                    taskID = Convert.ToInt32(dataReader["taskID"]),
                                    title = Convert.ToString(dataReader["title"]),
                                    hours = Convert.ToInt32(dataReader["hours"]),
                                    date = Convert.ToString(dataReader["date"]),
                                };
                            }
                        }
                        if (task == null)
                            throw new EntityNotFoundException();
                    }
                }
                return task;
            }
            catch (Exception)
            {
                throw new EntityNotFoundException();
            }
        }
    }
}
