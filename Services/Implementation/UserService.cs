using Timesheet.NETCore.Exceptions;
using Timesheet.NETCore.Models;
using System;
using System.Collections.Generic;
using System.Data;
using Timesheet.NETCore.Consts;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;

namespace Timesheet.NETCore.Services.Implementation
{
    public class UserService : IUserService
    {
        private readonly string _connectionString;
        private SqlQuery _sqlQuery;
        public UserService(IConfiguration Configuration)
        {
            _connectionString = Configuration["ConnectionString:DBCS"];
            _sqlQuery = new SqlQuery();
        }
        public User createUser(User user)
        {
            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    using (var sqlCmd = new SqlCommand(_sqlQuery.CREATE_USER, connection))
                    {
                        sqlCmd.Parameters.AddWithValue("accountID", user.accountID);
                        sqlCmd.Parameters.AddWithValue("firstName", user.firstName);
                        sqlCmd.Parameters.AddWithValue("middleName", user.middleName);
                        sqlCmd.Parameters.AddWithValue("lastName", user.lastName);
                        sqlCmd.Parameters.AddWithValue("gender", user.gender);
                        sqlCmd.Parameters.AddWithValue("birthDate", user.birthDate);
                        sqlCmd.Parameters.AddWithValue("address", user.address);
                        sqlCmd.ExecuteNonQuery();
                    }
                    using (var sqlCmd = new SqlCommand(_sqlQuery.SELECT_LAST_USER, connection))
                    {
                        User inserted = new User();
                        using (var dataReader = sqlCmd.ExecuteReader())
                        {
                            while (dataReader.Read())
                            {
                                inserted.userID = Convert.ToInt32(dataReader["userID"]);
                                inserted.accountID = Convert.ToInt32(dataReader["accountID"]);
                                inserted.firstName = Convert.ToString(dataReader["firstName"]);
                                inserted.middleName = Convert.ToString(dataReader["middleName"]);
                                inserted.lastName = Convert.ToString(dataReader["lastName"]);
                                inserted.gender = Convert.ToString(dataReader["gender"]);
                                inserted.birthDate = Convert.ToString(dataReader["birthDate"]);
                                inserted.address = Convert.ToString(dataReader["address"]);
                            }
                        }
                        return inserted;
                    }
                }
            }
            catch (Exception)
            {
                throw new BadRequestException();
            }
        }
        public List<User> getAllUsers()
        {
            try
            {
                List<User> result = new List<User>();
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    using (var sqlCmd = new SqlCommand(_sqlQuery.SELECT_ALL_USER, connection))
                    {
                        sqlCmd.CommandType = CommandType.Text;
                        using (var dataReader = sqlCmd.ExecuteReader())
                        {
                            while (dataReader.Read())
                            {
                                var user = new User
                                {
                                    userID = Convert.ToInt32(dataReader["userID"]),
                                    accountID = Convert.ToInt32(dataReader["accountID"]),
                                    firstName = Convert.ToString(dataReader["firstName"]),
                                    middleName = Convert.ToString(dataReader["middleName"]),
                                    lastName = Convert.ToString(dataReader["lastName"]),
                                    gender = Convert.ToString(dataReader["gender"]),
                                    birthDate = Convert.ToString(dataReader["birthDate"]),
                                    address = Convert.ToString(dataReader["address"])
                                };
                                result.Add(user);
                            }
                        }
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw new EntityNotFoundException();
            }
        }
        public void updateUser(User user)
        {
            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    using (var sqlCmd = new SqlCommand(_sqlQuery.UPDATE_USER, connection))
                    {
                        connection.Open();
                        sqlCmd.Connection = connection;
                        sqlCmd.Parameters.AddWithValue("id", user.userID);
                        sqlCmd.Parameters.AddWithValue("accountID", user.accountID);
                        sqlCmd.Parameters.AddWithValue("firstName", user.firstName);
                        sqlCmd.Parameters.AddWithValue("middleName", user.middleName);
                        sqlCmd.Parameters.AddWithValue("lastName", user.lastName);
                        sqlCmd.Parameters.AddWithValue("gender", user.gender);
                        sqlCmd.Parameters.AddWithValue("birthDate", user.birthDate);
                        sqlCmd.Parameters.AddWithValue("address", user.address);
                        int rowAffected = sqlCmd.ExecuteNonQuery();
                        if (rowAffected == 0)
                            throw new BadRequestException();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void deleteUser(int userID)
        {
            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    using (var sqlCmd = new SqlCommand(_sqlQuery.DELETE_USER))
                    {
                        connection.Open();
                        sqlCmd.Connection = connection;
                        sqlCmd.Parameters.AddWithValue("id", userID);
                        int rowAffected = sqlCmd.ExecuteNonQuery();
                        if (rowAffected == 0)
                            throw new BadRequestException();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public User getUserByID(int id)
        {
            try
            {
                User user = null;
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    using (var sqlCmd = new SqlCommand(_sqlQuery.SELECT_USER_BY_ID, connection))
                    {
                        sqlCmd.CommandType = CommandType.Text;
                        sqlCmd.Parameters.AddWithValue("id", id);
                        using (var dataReader = sqlCmd.ExecuteReader())
                        {
                            while (dataReader.Read())
                            {
                                user = new User
                                {
                                    userID = Convert.ToInt32(dataReader["userID"]),
                                    accountID = Convert.ToInt32(dataReader["accountID"]),
                                    firstName = Convert.ToString(dataReader["firstName"]),
                                    middleName = Convert.ToString(dataReader["middleName"]),
                                    lastName = Convert.ToString(dataReader["lastName"]),
                                    gender = Convert.ToString(dataReader["gender"]),
                                    birthDate = Convert.ToString(dataReader["birthDate"]),
                                    address = Convert.ToString(dataReader["address"]),
                                };
                            }
                        };
                        if (user == null)
                            throw new BadRequestException();
                    }
                }
                return user;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
