using Timesheet.NETCore.Exceptions;
using Timesheet.NETCore.Models;
using System;
using System.Collections.Generic;
using System.Data;
using Timesheet.NETCore.Consts;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;

namespace Timesheet.NETCore.Services.Implementation
{
    public class AppointmentService : IAppointmentService
    {
        private readonly string _connectionString;
        private SqlQuery _sqlQuery;
        public AppointmentService(IConfiguration Configuration)
        {
            _connectionString = Configuration["ConnectionString:DBCS"];
            _sqlQuery = new SqlQuery();
        }
        public Appointment createAppointment(Appointment appointment)
        {
            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    using (var sqlCmd = new SqlCommand(_sqlQuery.CREATE_APPOINTMENT, connection))
                    {
                        sqlCmd.Parameters.AddWithValue("patientID", appointment.patientID);
                        sqlCmd.Parameters.AddWithValue("doctorID", appointment.doctorID);
                        sqlCmd.Parameters.AddWithValue("scheduledDate", appointment.scheduledDate);
                        sqlCmd.Parameters.AddWithValue("notes", appointment.notes);
                        sqlCmd.ExecuteNonQuery();
                    }
                    using (var sqlCmd = new SqlCommand(_sqlQuery.SELECT_LAST_APPOINTMENT, connection))
                    {
                        Appointment inserted = new Appointment();
                        using (var dataReader = sqlCmd.ExecuteReader())
                        {
                            while (dataReader.Read())
                            {
                                inserted.appointmentID = Convert.ToInt32(dataReader["appointmentID"]);
                                inserted.patientID = Convert.ToInt32(dataReader["patientID"]);
                                inserted.doctorID = Convert.ToInt32(dataReader["doctorID"]);
                                inserted.scheduledDate = Convert.ToString(dataReader["scheduledDate"]);
                                inserted.notes = Convert.ToString(dataReader["notes"]);
                            }
                        }
                        return inserted;
                    }
                }
            }
            catch (Exception)
            {
                throw new BadRequestException();
            }
        }
        public List<Appointment> getAllAppointments()
        {
            try
            {
                List<Appointment> result = new List<Appointment>();
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    using (var sqlCmd = new SqlCommand(_sqlQuery.SELECT_ALL_APPOINTMENT, connection))
                    {
                        sqlCmd.CommandType = CommandType.Text;
                        using (var dataReader = sqlCmd.ExecuteReader())
                        {
                            while (dataReader.Read())
                            {
                                var appointment = new Appointment
                                {
                                    appointmentID = Convert.ToInt32(dataReader["appointmentID"]),
                                    patientID = Convert.ToInt32(dataReader["patientID"]),
                                    doctorID = Convert.ToInt32(dataReader["doctorID"]),
                                    scheduledDate = Convert.ToString(dataReader["scheduledDate"]),
                                    notes = Convert.ToString(dataReader["notes"]),
                                };
                                result.Add(appointment);
                            }
                        }
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw new EntityNotFoundException();
            }
        }
        public void updateAppointment(Appointment appointment)
        {
            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    using (var sqlCmd = new SqlCommand(_sqlQuery.UPDATE_APPOINTMENT, connection))
                    {
                        connection.Open();
                        sqlCmd.Connection = connection;
                        sqlCmd.Parameters.AddWithValue("id", appointment.appointmentID);
                        sqlCmd.Parameters.AddWithValue("patientID", appointment.patientID);
                        sqlCmd.Parameters.AddWithValue("doctorID", appointment.doctorID);
                        sqlCmd.Parameters.AddWithValue("scheduledDate", appointment.scheduledDate);
                        sqlCmd.Parameters.AddWithValue("notes", appointment.notes);
                        int rowAffected = sqlCmd.ExecuteNonQuery();
                        if (rowAffected == 0)
                            throw new BadRequestException();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void deleteAppointment(int appointmentID)
        {
            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    using (var sqlCmd = new SqlCommand(_sqlQuery.DELETE_APPOINTMENT))
                    {
                        connection.Open();
                        sqlCmd.Connection = connection;
                        sqlCmd.Parameters.AddWithValue("id", appointmentID);
                        int rowAffected = sqlCmd.ExecuteNonQuery();
                        if (rowAffected == 0)
                            throw new BadRequestException();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public Appointment getAppointmentByID(int id)
        {
            try
            {
                Appointment appointment = null;
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    using (var sqlCmd = new SqlCommand(_sqlQuery.SELECT_APPOINTMENT_BY_ID, connection))
                    {
                        sqlCmd.CommandType = CommandType.Text;
                        sqlCmd.Parameters.AddWithValue("id", id);
                        using (var dataReader = sqlCmd.ExecuteReader())
                        {
                            while (dataReader.Read())
                            {
                                appointment = new Appointment
                                {
                                    appointmentID = Convert.ToInt32(dataReader["appointmentID"]),
                                    patientID = Convert.ToInt32(dataReader["patientID"]),
                                    doctorID = Convert.ToInt32(dataReader["doctorID"]),
                                    scheduledDate = Convert.ToString(dataReader["scheduledDate"]),
                                    notes = Convert.ToString(dataReader["notes"])
                                };
                            }
                        };
                        if (appointment == null)
                            throw new BadRequestException();
                    }
                }
                return appointment;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
