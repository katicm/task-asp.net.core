using Timesheet.NETCore.Models;
using System.Collections.Generic;

namespace Timesheet.NETCore.Services
{
    public interface IDoctorService
    {
        Doctor createDoctor(Doctor doctor);
        void updateDoctor(Doctor doctor);
        void deleteDoctor(int doctorID);
        List<Doctor> getAllDoctors();
        Doctor getDoctorByID(int id);
    }
}