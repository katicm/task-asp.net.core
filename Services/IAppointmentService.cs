using Timesheet.NETCore.Models;
using System.Collections.Generic;

namespace Timesheet.NETCore.Services
{
    public interface IAppointmentService
    {
        Appointment createAppointment(Appointment appointment);
        void updateAppointment(Appointment appointment);
        void deleteAppointment(int appointmentID);
        List<Appointment> getAllAppointments();
        Appointment getAppointmentByID(int id);
    }
}