using Timesheet.NETCore.Models;
using System.Collections.Generic;

namespace Timesheet.NETCore.Services
{
    public interface IUserService
    {
        User createUser(User user);
        void updateUser(User user);
        void deleteUser(int userID);
        List<User> getAllUsers();
        User getUserByID(int id);
    }
}