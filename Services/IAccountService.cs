using Timesheet.NETCore.Models;
using System.Collections.Generic;

namespace Timesheet.NETCore.Services
{
    public interface IAccountService
    {
        Account createAccount(Account account);
        void updateAccount(Account account);
        void deleteAccount(int accountID);
        List<Account> getAllAccounts();
        Account getAccountByID(int id);
        Account login(string email, string password);
    }
}